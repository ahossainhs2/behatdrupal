Feature: create, edit  article content type
  I order to article content type
  authenticated user
  user creates article successfully

  @reg
  Scenario: user creates article content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Article"
    And I fill in the following:
      |edit-title-0-value|Test Article|
      |edit-body-0-value|This is a test body. Please ignore|
      |edit-field-tags-target-id|test|
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

  @reg
  Scenario: user edits article content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Article" from "type"
    And I fill in "Test Article" for "title"
    And I press "Filter"
    And I click "Test Article"
    And I should see "Test Article"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in the following:
      |edit-title-0-value|Test Article edit|
      |edit-body-0-value|This is a test body. Please ignore edit|
    And press "Save and keep published"
    And I should see "has been updated."
  @reg
  Scenario: user deletes article content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Article" from "type"
    And I fill in "Test Article" for "title"
    And I press "Filter"
    And I click "Test Article"
    And I should see "Test Article"
    And I should see "Submitted by"
    And I click "Delete"
    And I should see "This action cannot be undone."
    And press "Delete"
    And I should see "has been deleted."
