Feature: create, edit & delete event session content type
  In order to create, edit & delete a event content type
  authenticated user
  user creates event successfully

  @reg
  Scenario: user creates event session content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Event Session"
    And I should see "Create Event Session"
    And I select "Breastfeeding Support Group at TCMC" from "Event Parent"
    And I select "Tennova Healthcare" from "Facility Group"
    And I select "Turkey Creek Medical Center" from "Facility"
    And I select the radio button "Open" with the id "edit-field-session-status-open"
    And I check "Online"
    And I fill in the following:
      |Title        |Test Event session|
      |Session Size|20|
      |edit-field-session-instructor-0-first-name|Test event|
      |edit-field-session-instructor-0-last-name| Instructor|
      |Email| anowar.hossain@hs2solutions.com|
      |edit-field-session-instructor-0-phone|312-232-2322|
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

  @reg
  Scenario: user edits event session content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Event Session" from "type"
    And I fill in "Test Event session" for "title"
    And I press "Filter"
    And I click "Test Event session"
    And I should see "Test Event session"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in "Test Event session edit" for "Title"
    And I check "Phone"
    And I fill in "(402) 232-2322" for "edit-field-session-reg-phone-0-value"
    And I fill in "30" for "Session Size"
    And fill in "(312) 232-2322" for "edit-field-session-instructor-0-phone"
    And press "Save and keep published"
    And I should see "has been updated."
    And I should see "Session Instructor"
    And I should see "(312) 232-2322"
  @reg
  Scenario: user deletes event session  content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Event Session" from "type"
    And I fill in "Test Event session edit" for "title"
    And I press "Filter"
    And I click "Test Event session edit"
    And I should see "Test Event session edit"
    And I should see "Submitted by"
    And I click "Delete"
    And I should see "This action cannot be undone."
    And press "Delete"
    And I should see "has been deleted."
