Feature: user logout successfully
  I order to logout from the homepage
  authenticated user
  user lgout successfully

  @smoke @reg

  Scenario: user logout successfully
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    And I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    When I click "My account"
    And  I click "Log out"
    Then I should see the button "Log in"
