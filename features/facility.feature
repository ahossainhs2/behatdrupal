Feature: create, edit  facility  content type
  I order to article content type
  authenticated user
  user creates article successfully

  @reg
  Scenario: user creates facility content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Facility"
    And I select "Tennova Healthcare" from "Facility Group"
    And I fill in the following:
      |edit-title-0-value|Test Facility|
      |Facility Telephone |402 517 0672                    |
      |Fax Number         |485 373 9393                    |
      |Facility Code      |303                             |
      |edit-field-facility-type-0-target-id| Hospital      |
      |Street address                      |100 lake dr    |
      |Additional                          |Additional info|
      |City                                |Brooklyn       |
      |Postal code                         |11230          |
    And I select "New York" from "State/Province"
    And I fill in the following:
      |edit-field-facility-hours-value-0-starthours-hour  |8|
      |edit-field-facility-hours-value-0-starthours-minute|30|
      |edit-field-facility-hours-value-0-endhours-hour    |7|
      |edit-field-facility-hours-value-0-endhours-minute  |30|
    And I fill in the following:
      |edit-field-facility-hours-value-1-starthours-hour  |13|
      |edit-field-facility-hours-value-1-starthours-minute|30|
      |edit-field-facility-hours-value-1-endhours-hour    |17|
      |edit-field-facility-hours-value-1-endhours-minute  |30|
    And I fill in the following:
      |edit-field-facility-hours-value-2-starthours-hour  |13|
      |edit-field-facility-hours-value-2-starthours-minute|30|
      |edit-field-facility-hours-value-2-endhours-hour    |17|
      |edit-field-facility-hours-value-2-endhours-minute  |30|
    And press "Save and publish"
    And I should see "Sunday: Closing hours are earlier than Opening hours."
    And I fill in "11" for "edit-field-facility-hours-value-0-endhours-hour"
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"
    And I should see "40.6"
    And I should see "-73"

  @reg
  Scenario: user edits facility content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Facility" from "type"
    And I fill in "Test Facility" for "title"
    And I press "Filter"
    And I click "Test Facility"
    And I should see "Test Facility"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in the following:
      |edit-title-0-value|Test Facility edit|
      |edit-body-0-value|This is a test body. Please ignore edit|
    And press "Save and keep published"
    And I should see "has been updated."

  @reg
  Scenario: user deletes facility content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Facility" from "type"
    And I fill in "Test Facility edit" for "title"
    And I press "Filter"
    And I click "Test Facility edit"
    And I should see "Test Facility edit"
    And I should see "Submitted by"
    And I click "Delete"
    And I should see "This action cannot be undone."
    And press "Delete"
    And I should see "has been deleted."
