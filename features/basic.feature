Feature: create, edit  basic content type
  I order to manage basic content type
  authenticated user
  user creates,edits, delete basic successfully

  @smoke @reg
  Scenario: user creates basic content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Basic page"
    And I should see "Create Basic page"
    And press "Save and publish"
    And I fill in the following:
      |Title            |Test Basic page                   |
      |edit-body-0-value|This is a test body. Please ignore|
    And press "Save and publish"
    And I should see "has been created."

  @reg
  Scenario: user edits article content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Basic page" from "type"
    And I fill in "Test Basic page" for "title"
    And I press "Filter"
    And I click "Test Basic page"
    And I should see "Test Basic page"
    And I click "Edit"
    And I fill in the following:
      |edit-title-0-value|Test Basic page edit|
      |edit-body-0-value|This is a test body. Please ignore edit|
    And press "Save and keep published"
    And I should see "has been updated."
    
  @reg
  Scenario: user deletes article content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Basic page" from "type"
    And I fill in "Test Basic page edit" for "title"
    And I press "Filter"
    And I click "Test Basic page edit"
    And I should see "Test Basic page edit"
    And I click "Delete"
    And I should see "This action cannot be undone."
    And press "Delete"
    And I should see "has been deleted."