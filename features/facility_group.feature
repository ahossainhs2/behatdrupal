Feature: create, edit  facility group content type
  I order to article content type
  authenticated user
  user creates article successfully

@reg
  Scenario: user creates facility group content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Facility Group"
    And I fill in the following:
      |edit-title-0-value|Test Facility Group|
      |edit-body-0-value|This is a test body. Please ignore|
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

  @reg
  Scenario: user edits facility group content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Facility Group" from "type"
    And I fill in "Test Facility Group" for "title"
    And I press "Filter"
    And I click "Test Facility Group"
    And I should see "Test Facility Group"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in the following:
      |edit-title-0-value|Test Facility Group edit|
      |edit-body-0-value|This is a test body. Please ignore edit|
    And press "Save and keep published"
    And I should see "has been updated."


