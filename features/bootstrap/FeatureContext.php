<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Event\ScenarioEvent;
use Behat\MinkExtension\Context\MinkContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

    /**
     * @Given /^I set browser window size to "([^"]*)" x "([^"]*)"$/
     */
    public function iSetBrowserWindowSizeToX($width, $height) {
        $this->getSession()->resizeWindow((int)$width, (int)$height, 'current');
    }

    public function coursedate()
    {
        $this->getSession()->getPage()->find("css", "input[name='coursedate']")->setValue("your_date");
    }

    /**
     * Click some text
     *
     * @When /^I click on the text "([^"]*)"$/
     */
    public function iClickOnTheText($text)
    {
        $session = $this->getSession();
        $element = $session->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath('xpath', '*//*[text()="'. $text .'"]')
        );
        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $text));
        }

        $element->click();

    }

    /**
     * @Then /^I click on "([^"]*)"$/
     */
    public function iClickOn($element)
    {
        $page = $this->getSession()->getPage();
        $findName = $page->find("css", $element);
        if (!$findName) {
            throw new Exception($element . " could not be found");
        } else {
            $findName->click();
        }
    }

    /**
     * Checks, that form element with specified label is visible on page.
     *
     * @Then /^(?:|I )should see an? "(?P<label>[^"]*)" form element$/
     */
    public function assertFormElementOnPage($label) {
        $element = $this->getSession()->getPage();
        $nodes = $element->findAll('css', '.form-item label');
        foreach ($nodes as $node) {
            if ($node->getText() === $label) {
                if ($node->isVisible()) {
                    return;
                }
                else {
                    throw new \Exception("Form item with label \"$label\" not visible.");
                }
            }
        }
        throw new \Behat\Mink\Exception\ElementNotFoundException($this->getSession(), 'form item', 'label', $label);
    }
    /**
     * Checks, that form element with specified label and type is visible on page.
     *
     * @Then /^(?:|I )should see an? "(?P<label>[^"]*)" (?P<type>[^"]*) form element$/
     */
    public function assertTypedFormElementOnPage($label, $type) {
        $container = $this->getSession()->getPage();
        $pattern = '/(^| )form-type-' . preg_quote($type). '($| )/';
        $label_nodes = $container->findAll('css', '.form-item label');
        foreach ($label_nodes as $label_node) {
            // Note: getText() will return an empty string when using Selenium2D. This
            // is ok since it will cause a failed step.
            if ($label_node->getText() === $label
                && preg_match($pattern, $label_node->getParent()->getAttribute('class'))
                && $label_node->isVisible()) {
                return;
            }
        }
        throw new \Behat\Mink\Exception\ElementNotFoundException($this->getSession(), $type . ' form item', 'label', $label);
    }
    /**
     * Checks, that element with specified CSS is not visible on page.
     *
     * @Then /^(?:|I )should not see an? "(?P<label>[^"]*)" form element$/
     */
    public function assertFormElementNotOnPage($label) {
        $element = $this->getSession()->getPage();
        $nodes = $element->findAll('css', '.form-item label');
        foreach ($nodes as $node) {
            // Note: getText() will return an empty string when using Selenium2D. This
            // is ok since it will cause a failed step.
            if ($node->getText() === $label && $node->isVisible()) {
                throw new \Exception();
            }
        }
    }
    /**
     * Checks, that form element with specified label and type is not visible on page.
     *
     * @Then /^(?:|I )should not see an? "(?P<label>[^"]*)" (?P<type>[^"]*) form element$/
     */
    public function assertTypedFormElementNotOnPage($label, $type) {
        $container = $this->getSession()->getPage();
        $pattern = '/(^| )form-type-' . preg_quote($type). '($| )/';
        $label_nodes = $container->findAll('css', '.form-item label');
        foreach ($label_nodes as $label_node) {
            // Note: getText() will return an empty string when using Selenium2D. This
            // is ok since it will cause a failed step.
            if ($label_node->getText() === $label
                && preg_match($pattern, $label_node->getParent()->getAttribute('class'))
                && $label_node->isVisible()) {
                throw new \Behat\Mink\Exception\ElementNotFoundException($this->getSession(), $type . ' form item', 'label', $label);
            }
        }
    }
}
