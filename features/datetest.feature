Feature: This is a test. Ignore
  In order to create, edit & delete a event content type
  authenticated user
  user creates event successfully



  Scenario: Test test test
    Given I am on the homepage
    And I click "Log in"
    And I should see "Log in"
    And I fill in the following:
    |Username|admin|
    |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Event Session Part"
    And I should see "Create Event Session Part"
    And I fill in "Title" with "Test Event session part"
    And I select "Breastfeeding Support Group at TCMC" from "Event Parent"
    And I select "Test Event session edit" from "Event Session"
    And I fill in "edit-field-session-part-start-time-0-value-date" with ""
    And I click "2"
    And I fill in "edit-field-session-part-start-time-0-value-time" with "10:00"
    And I fill in "edit-field-session-part-end-time-0-value-date" with ""
    And I click "27"
    And I fill in "edit-field-session-part-end-time-0-value-time" with "17:00"
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"