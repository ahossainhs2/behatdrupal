Feature: create, edit  event category content type
  I order to article content type
  authenticated user
  user creates article successfully

  @reg
  Scenario: user creates event category content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Event Category"
    And I should see "Create Event Category"
    And I fill in the following:
      |Title        |Test Event Category|
      |edit-body-0-value|This is a test body. Please ignore|
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

  @reg
  Scenario: user edits event category content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Event Category" from "type"
    And I fill in "Test Event Category" for "title"
    And I press "Filter"
    And I click "Test Event Category"
    And I should see "Test Event Category"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in the following:
      |Title        |Test Event Category Edit|
    And press "Save and keep published"
    And I should see "has been updated."