Feature: create, edit & delete event session part content type
  In order to create, edit & delete a event content type
  authenticated user
  user creates event successfully


  @reg
  Scenario: user creates event session content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Event Session"
    And I should see "Create Event Session"
    And I select "Breastfeeding Support Group at TCMC" from "Event Parent"
    And I select "Tennova Healthcare" from "Facility Group"
    And I select "Turkey Creek Medical Center" from "Facility"
    And I select the radio button "Open" with the id "edit-field-session-status-open"
    And I check "Online"
    And I fill in the following:
      |Title        |Test Event session edit|
      |Session Size|20|
      |edit-field-session-instructor-0-first-name|Test event|
      |edit-field-session-instructor-0-last-name| Instructor|
      |Email| anowar.hossain@hs2solutions.com|
      |edit-field-session-instructor-0-phone|312-232-2322|
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

  @reg
  Scenario: user creates event session part content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Event Session Part"
    And I should see "Create Event Session Part"
    And I fill in "Title" with "Test Event session part"
    And I select "Breastfeeding Support Group at TCMC" from "Event Parent"
    And I select "Test Event session edit" from "Event Session"
    And I fill in the following:
      |edit-field-session-part-start-time-0-value-date| 2016-01-01|
      |edit-field-session-part-start-time-0-value-time| 10:00     |
      |edit-field-session-part-end-time-0-value-date  | 2016-12-01|
      |edit-field-session-part-end-time-0-value-time  | 17:00     |
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

