Feature: Make the Home page the login Screen
  In order to see login page as homepage
  annonymous user
  Login page fields displayed on homepage : https://hs2studio.jira.com/browse/CC-65

@smoke @reg
  Scenario: Make homepage the login screen 
    Given I am on the homepage
    Then I should see the text "Log in" in the "header"
    And I should see the link "Home"
    And I should see "Enter your CRM Central username."
    And I should see "Enter the password that accompanies your username."
    And I should see the button "Log in"



