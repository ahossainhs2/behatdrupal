Feature: create, edit & delete event content type
  In order to create, edit & delete a event content type
  authenticated user
  user creates event successfully
@reg
  Scenario: user creates event  content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
    |Username|admin|
    |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Event"
    And I should see "Create Event"
    And I select "Tennova Healthcare" from "Facility Group"
    And I fill in the following:
    |Title        |Test Event|
    |edit-field-event-manager-0-first-name|Test event|
    |edit-field-event-manager-0-last-name| Manager|
    |Email| anowar.hossain@hs2solutions.com|
    |Phone|312-232-2322|
    |edit-field-event-cost-0-value|10|
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

  @reg
  Scenario: user edits event  content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Event" from "type"
    And I fill in "Test Event" for "title"
    And I press "Filter"
    And I click "Test Event"
    And I should see "Test Event"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in the following:
      |Title        |Test Event edit|
    And press "Save and keep published"
    And I should see "has been updated."

  @reg
  Scenario: user delets event  content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Event" from "type"
    And I fill in "Test Event edit" for "title"
    And I press "Filter"
    And I click "Test Event"
    And I should see "Test Event"
    And I should see "Submitted by"
    And I click "Delete"
    And I should see "This action cannot be undone."
    And press "Delete"
    And I should see "has been deleted."