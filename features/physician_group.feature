Feature: create, edit  physician group content type
  I order to article content type
  authenticated user
  user creates article successfully

  @reg
  Scenario: user creates Physician group content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Physician Group"
    And I should see "Create Physician Group"
    And I fill in the following:
      |Title        |Test Physician Group|
      |edit-body-0-value|This is a test body. Please ignore|
      |URL              |http://tennova.com|
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

  @reg
  Scenario: user edits physician group content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Physician Group" from "type"
    And I fill in "Test Physician Group" for "title"
    And I press "Filter"
    And I click "Test Physician Group"
    And I should see "Test Physician Group"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in the following:
      |Title        |Test Physician Group edit|
      |edit-body-0-value|This is a test body. Please ignore|
      |URL              |http://wuesthoff.com|
    And press "Save and keep published"
    And I should see "has been updated."

  @reg
  Scenario: user deletes physician group content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Physician Group" from "type"
    And I fill in "Test Physician Group edit" for "title"
    And I press "Filter"
    And I click "Test Physician Group edit"
    And I should see "Test Physician Group"
    And I should see "Submitted by"
    And I click "Delete"
    And I should see "This action cannot be undone."
    And press "Delete"
    And I should see "has been deleted."
