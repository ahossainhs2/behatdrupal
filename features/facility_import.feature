Feature: admin able to import facilities data
  In order to import facilities data
  admin user
  user imports facilities successfully

  @reg
  Scenario: user imports facilities data
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    And I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    When I click "CRM Central"
    Then I should see "CRM Configuration"
    And I press "Sync All Facilities"
    And I should see "Facility import was run successfully. There were"
    And I should not see "Unable to geocode address, missing address components."

  @reg
  Scenario: user look for facility data import
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Facility" from "type"
    And I fill in "Colorado Health Neighborhoods" for "title"
    And I press "Filter"
    And I click "Colorado Health Neighborhoods"
    And I should see "Colorado Health Neighborhoods"
    And I should see "Submitted by"
    And I click "Edit"
