Feature: delete  facility group content type
  In order to delete facility group content type
  authenticated user
  user delete facility group successfully

  Scenario: user deletes facility group content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Facility Group" from "type"
    And I fill in "Test Facility Group" for "title"
    And I press "Filter"
    And I click "Test Facility Group"
    And I should see "Test Facility Group"
    And I should see "Submitted by"
    And I click "Delete"
    And I should see "This action cannot be undone."
    And press "Delete"
    And I should see "has been deleted."


    And I check the box "edit-field-event-payment-methods-0'

  |edit-field-event-manager-0-first-name|Test event|
  |edit-field-event-manager-0-last-name| Manager|
  |Email| anowar.hossain@hs2solutions.com|
  |Phone|312-232-2322|
  |Event Cost| $10|
  |Event Cost Details|Event cost will cover cost of room, event materials and refreshments|
  |Event Payment Information|Event info: Please come on time.|
  |Additional Information|Adlditiona info: Do not park in the employee parking. There is enough parking space in the parking lot to the north.|
  And I select "0" from "edit-field-event-payment-methods-0"



  Scenario: user edits event  content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    And I select "Event" from "type"
    And I fill in "Test Event" for "title"
    And I press "Filter"
    And I click "Test Event"
    And I should see "Test Event"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in the following:
      |Title        |Test Event edit|
    And press "Save and keep published"
    And I should see "has been updated."