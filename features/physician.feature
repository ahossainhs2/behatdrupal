Feature: create, edit  physician content type
  I order to article content type
  authenticated user
  user creates article successfully

  @reg
  Scenario: user creates Physician content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Physician"
    And I should see "Create Physician"
    And press "Save and publish"
    And I should see the text "First Name field is required."
    And I should see the text "Last Name field is required."
    And I should see the text "Title field is required."
    And I fill in the following:
      |Title                                         |Test Physician                    |
      |Salutation                                    |Mr.                               |
      |First Name                                    |Jhon                              |
      |Last Name                                     |Doe                               |
      |Middle Name                                   |Z                                 |
      |Suffix                                        |Jr.                               |
      |edit-body-0-value                             |This is a test body. Please ignore|
      |edit-field-birth-date-0-value-date            |1978-08-10                        |
      |edit-field-physician-specialty-0-target-id    |Orthopaedic Surgery               |
      |edit-field-physician-sub-specialty-0-target-id|Surgery - Spine                   |
      |edit-field-admin-specialty-0-target-id        |Internal Medicine                 |
    And I select the radio button "Male"
    And I select "Turkey Creek Medical Center" from "Facility"
    And press "Save and publish"
    And I should see "has been created."
    And I should see "Submitted by"

  @reg
  Scenario: user edits physician content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    And I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    When I select "Physician" from "type"
    And I fill in "Test Physician" for "title"
    And I press "Filter"
    And I click "Test Physician"
    And I should see "Test Physician"
    And I should see "Submitted by"
    And I click "Edit"
    And I fill in the following:
      |Title        |Test Physician Edit|
      |Salutation   |Mr.                |
      |First Name   |Jhones             |
      |Last Name    |Doe                |
      |Email        |test@test.com      |
      |URL          |http://example.com |
      |Link text    |Website            |
      |Physician NPI|6779               |
    And I select "Fellowship" from "Physician Background Type"
    And I fill in the following:
    |Physician Background Info|Physician Background Info|
    |Publication Title        |Publiation title         |
    |edit-field-physician-publications-0-field-publication-url-0-uri|http://www.publications.com   |
    |edit-field-physician-publications-0-field-publication-url-0-title| publication link text|
    |Publication Description                                          | Publication description|
    |Testimonial Title                                                |Testimonial Title       |
    |Testimonial Description                                          |Testimonial description |
    |Clinical Trial Title                                             |Clinical trial title    |
    |edit-field-physician-clinical-trials-0-field-clinical-trial-url-0-uri  |http://www.clinicaltrials.com/|
    |edit-field-physician-clinical-trials-0-field-clinical-trial-url-0-title|Clinical Trial link text|
    |Clinical Trial Description                                             | Clinical Trial Description|
    |Search Tags                                                            |physician, best physician  |
    And I fill in "Physician Background Info" with "Physician Background Info"
    And I press "Save and keep published"
    Then I should see "has been updated."
    And I should see "Orthopaedic Surgery"
    And I should see "Surgery - Spine"
    And I should see "Search Tags"
    And I should see "Physician Background Type"
    And I should see "Physician Background Info"
    And I should see "Publiation title"
    And I should see "Publication URL"
    And I should see "Testimonial Title"
    And I should see "Testimonial description"
    And I should see "Clinical trial title"
    And I should see "Clinical Trial Description"





  @reg
  Scenario: user deletes physician content type
    Given I am on the homepage
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    And I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/admin/content"
    And I should see "Content"
    When I select "Physician" from "type"
    And I fill in "Test Physician" for "title"
    And I press "Filter"
    And I click "Test Physician"
    Then I should see "Test Physician"
    And I should see "Submitted by"
    And I click "Delete"
    And I should see "This action cannot be undone."
    And press "Delete"
    And I should see "has been deleted."
