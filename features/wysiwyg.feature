Feature: create, edit  basic content type
  I order to manage basic content type
  authenticated user
  user creates,edits, delete basic successfully

  @smoke @reg @javascript
  Scenario: user creates basic content type
    Given I am on the homepage
    And I click "Log in"
    And I should see "Log in"
    And I fill in the following:
      |Username|admin|
      |Password|admin|
    And press "edit-submit"
    Then I should see the link "Contact"
    And I should see the text "Log out"
    And I should see the link "My account"
    And I go to "/node/add"
    And I should see "Add content"
    And I click "Basic page"
    And I should see "Create Basic page"
    And press "Save and publish"
    And I fill in the following:
      |Title            |Test Basic page                   |
    And press "Save and publish"
    And I should see "has been created."