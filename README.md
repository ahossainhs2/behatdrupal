# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

To get started writing automated testing using Behat and mink framework for Drupal site checkout this repo. 


* [Learn more](https://hs2studio.jira.com/wiki/display/HS2WIKI/Drupal+Extension+to+Behat+and+Mink)

### How do I get set up? ###

Repo setup:

1. Clone the repo : git clone git@bitbucket.org:ahossainhs2/behatdrupal.git
2. cd into the Behat folder


Setting up Behat and mink with Drupal extenion: 
1. Install composer: 

 curl -sS https://getcomposer.org/installer 
 php mv composer.phar /usr/local/bin/composer

2. cd into repo folder "behat"
 
3. Execute the install command. It will install all a library mentioned in the composer.json file     
  sudo composer.phar install

4. Verify that installation was completed successfully by running 
   bin/behat --help

5. Make the installation available system wide
   ln -s /opt/drupalextension/bin/behat /usr/local/bin/behat


Setting the test:

 1. review behat.yml file to update base_url to your drupal site

 
 2. lets initialize behat. This will create feature folder 
    bin/behat --init
 This will generate featureContext.php file making it aware of the Drupal and mink extension. It will allow you to the use of their drivers and step definitions and add your own custom step definitions. 

3. Verify test setup is successfully :
behat -dl
It should display behat definitions that comes with behat and mink 



### Who do I talk to? ###

* Repo owner or admin : Anowar Hossain